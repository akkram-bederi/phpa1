<?php require_once "./a1Code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>

	<div style="border:1px solid black; width:300px">
		<h1>Full Address</h1>
		<p><?php echo getFullAddress("Philippines","Manila","Metro Manila","1810 Antonio Rivera Street") ?> </p>

		<h1>Letter-Based Grading</h1>
		<p><?php echo '87 is equivalent to ' .getGrade(87) ?></p>
		<p><?php echo '94 is equivalent to ' .getGrade(94) ?></p>
		<p><?php echo '74 is equivalent to ' .getGrade(74) ?></p>
	</div>
	
</body>
</html>