<?php

/*
[SECTION] Comments
*/

$name="Akkram Bederi";
$email="akkram@mail.com";

$name="Akkram Not Bederi";

//Constants used to hold data that are meant to be read only values cannot be changed 
//Constants are defined using the define() function

define("PI",3.1416);

/*
Syntax:
defined(var_name,var_value)

By convention, constant names are in all caps

[Sections] Data Types
*/

$state="Manila";
$country='Philippines';
$address= $state .', ' .$country;
$address="$state, $country";

//Integers

$age=23;
$headcount=26;

//Floates
$grade = 98.2;
$distanceInKilometeres=1342.12;

//Boolean
$hasTraveledAbroad=false;
$hasSymptoms=true;

//Null
$spouse=null;

//Objects

$person=(object)[
	'fullName'=>'Akkram Bederi',
	'isMarred' => false,
	'age'=> 23,
	'address'=> (object)[
		'state'=>'New York',
		'country'=>'Philippines'
	]
];

//Arrays
$grades=array(99,99,98,99,99);

/*
[SECTIONS] Operators

Assignemtn Operator
	-Used to assign values to variables
*/

	$x=1234;
	$y=12345;