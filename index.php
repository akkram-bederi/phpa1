<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S1: PHP Basics and Selection Control Structures</title>
</head>
<body>
		
		<h1>Echoing Values</h1>
		<p><?php echo "Good day " . $name . "! Your given email is " . $email . "."; ?></p>
		<p><?php echo "Good day $name! Your given email is $email."; ?></p>
		<p><?php echo PI ?></p>
		<p><?php echo "Your address is $address." ?></p>
		<!-- Booleans and null values will not be visible in web page -->
		<p><?php echo $hasTraveledAbroad; ?></p>
		<p><?php echo $spouse; ?></p>
		<!-- In order to print them out, we can use var_dump -->
		<p><?php echo var_dump($spouse)?></p>

		<!-- To see a values data type, we can use gettype() -->
		<p><?php echo gettype($hasTraveledAbroad); ?></p>

		<!-- To output the value of an object property, the arrow notation can be used -->

		<p><?php echo $person->address->state; ?></p>

		<!-- To output an arrray element, we can use the usual square brackets -->
		<p><?php echo $grades[3] ?></p>

		<p>Sum: <?php echo $x + $y; ?></p>
		<p>Difference: <?php echo $x - $y; ?></p>
		<p>Product: <?php echo $x * $y; ?></p>
		<p>Quotient: <?php echo $x / $y; ?></p>

		<h2>Equality Operators</h2>
		<p>Loose Equality: <?php echo var_dump($x=='1234');?></p>
		<p>Loose Equality: <?php echo var_dump($x==='1234');?></p>

		<!-- Comparison Operators
		is less < 
		is greter >
		less or equal <=
		greater than or equal =>
		


		 -->
</body>
</html>